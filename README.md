# eBay Scammer

The list is a small collection of known scammers, sadly eBay and eBay-Kleinanzeigen usually keeping those scammer accounts online even after you report them with evidence. So this list here is designed to warn potential buyers and not meant to randomly accuse people.


I do this project in my free time based on reports.


## Submitting pull requests

You are able to submit pull requests but only with evidence, simply to avoid fake reports.


## Potential protection against scammers

- Assuming you use PayPal do not use friend money transfer because in this case it is unlikely you get your money back even if you report someone. This option is designed for family members that you trust and not random strangers. People usually prefer the options to avoid fees, but this can be fatal since there is no legal protection because it is automatically assumed you use this with actual friends and family members only.
- Check other products on the same website, if its way below the ordinary price then it is usually a scam to make you believe that this is a good deal, to motivate you to buy and fall the product.
- Check the username online, if it sounds made up or weird then stay away from the deal.
- Check the text if it is copied. There are bunch of engines to check for plagiarism. The chance is high that this is a scam, but keep in mind that this is not an absolute measurement instrument.
- Do a image reverse search to see if the pictures are stolen or copied from someone else.
- Never sell with your bank account use PayPal. There are scammer that close their PayPal account shortly after, so this is not entirely a perfect solution but creates more effort for scammers.
- Visit the seller, if possible and check the product with your own eyes.
- If there is absolute no reference, history or any results from the user online than this might be a fake account. Stay away from such accounts and deals they offer.


## eBay


## eBay-Kleinanzeigen
- [Friese Alonzo](https://www.ebay-kleinanzeigen.de/s-bestandsliste.html?userId=2272108)
- [Miri](https://www.ebay-kleinanzeigen.de/s-anzeige/tokyo-ghoul-re-band-1-13-achtung-scammer/2148684503-284-18276)


